# SuperRF_Dataset

This is the dataset released with paper "SuperRF: Enhanced 3D RF Representation Using Stationary Low-Cost mmWave Radar" that appeared in EWSN 2020.

The devices used in the dataset collection: 
```
Kinect V1
Ti mmWave AWR 1443 EVM
```

The dataset is organized as follow:

```
README.md
SuperRF_Data
|-- color_img
|-- depth_img
|-- intrinsics
|-- dataset_example.ipynb
|-- superrf_data.h5
```

The **color_img** folder contains the color image (in ppm format) captured by the Kinect. Folder **depth_img** contains the depth image (in pgm format) captured by the Kinect. The intrinsics of the Kinect is contained in the **intrinsics** folder.

The dataset is stored in the **superrf_data.h5** file with HDF5 format. The structure of the dataset is:

```
Item_name
|-RF_raw
|-NLOS
|-distance
|-color_img
|-depth_img
|-intrinsics
```

The **RF_raw** is the Ti mmWave radar data from doppler bin 0 (static). The data is stored in 3D matrix format ``(num_scans, dis, Tx-Rx value)``.  Each ``num_scans`` index represent a height where the mmWave radar took a snapshot of the environment, and two adjacent index means the mmWave radar is half the wavelength (2 mm in our case). Each index in ``dis`` dimension represents the distance to the radar. Adjacent index in the ``dis`` dimension means 4 cm apart. ``Tx-Rx value`` is (real, imag, real, imag, ... , real, imag) format with (Tx1 - Rx1, Tx1 - Rx2, ... , Tx3 - Rx4) sequence. For example, access the two azimuth antenna, use the first 16 numbers to construct the complex radar response. 

**NLOS** is if this item is non-line-of-sight situation, with **1** means yes. **distance** is the general distance between the item and radar. **color_img**, **depth_img**, and **intrinsics** store the name of the each file corresopnding to the current item.

Example usages of the data is presented in Python notebook file **dataset_example.ipynb**. It shows how to access each data field and construct the spectrum image of the scene.

For usage of the dataset, please cite:

```
To be added, waiting index on the paper.
```

Thanks!